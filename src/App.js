import NewComponents from "./components/newComponent";
import styles from "./styles/home.module.scss";
import { useState } from "react";

function App() {
  const [counter, setCounter] = useState(0);
  const [name, setName] = useState("");
  return (
    <div className={styles["homeTitle"]}>
      <h1>This is a new Page</h1>
      <p>A paragraph</p>
      <button>Submit</button>
      <div>
        <h2>Counter</h2>
        <button
          data-testid="add"
          onClick={() => setCounter((prevState) => prevState + 1)}
        >
          +
        </button>
        <h3 data-testid="counter">Counter: {counter}</h3>
        <button
          data-testid="subtract"
          onClick={() => setCounter((prevState) => prevState - 1)}
        >
          -
        </button>
      </div>
      <div>
        <input
          type="text"
          placeholder="input your name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        {name ? <p data-testid="name">Hello, my name is {name}</p> : null}
      </div>
      <NewComponents data={"this is a props from App"} />
    </div>
  );
}

export default App;
