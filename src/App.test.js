import {
  render,
  screen,
  fireEvent,
  waitForElementToBeRemoved,
} from "@testing-library/react";
import user from "@testing-library/user-event";
import App from "./App";
import NewComponents from "./components/newComponent";

test("renders this is a new page", () => {
  // cek jika ada tulisan this is a new page dan a paragraph di komponen App
  render(<App />);
  const linkElement = screen.getByText(/this is a new page/i);
  expect(linkElement).toBeInTheDocument();
  const paragraph = screen.getByText(/a paragraph/i);
  expect(paragraph).toBeInTheDocument();
});

it("render paragraph", () => {
  // cek jika ada tulisan this is a new component di component NewComponents
  render(<NewComponents />);
  const title = screen.getByText(/this is a new component/i);
  expect(title).toBeInTheDocument();
});

it("renders button", () => {
  // cek jika ada tombol dengan tulisan submit di component App
  render(<App />);
  const button = screen.getByText(/submit/i);
  expect(button).toBeInTheDocument();
});

it("renders counter correctly", () => {
  // cek jika tombol + dan - sesuai fungsinya
  render(<App />);
  const plus = screen.getByTestId("add");
  const minus = screen.getByTestId("subtract");
  const counter = screen.getByTestId("counter");

  expect(counter).toHaveTextContent("Counter: 0");
  fireEvent.click(plus);
  expect(counter).toHaveTextContent("Counter: 1");
  fireEvent.click(plus);
  expect(counter).toHaveTextContent("Counter: 2");
  fireEvent.click(plus);
  expect(counter).toHaveTextContent("Counter: 3");
  fireEvent.click(minus);
  expect(counter).toHaveTextContent("Counter: 2");
  fireEvent.click(minus);
  expect(counter).toHaveTextContent("Counter: 1");
});

it("renders input correctly", () => {
  // cek jika input akan memunculkan text
  const { getByText } = render(<App />);
  const input = screen.getByPlaceholderText(/input your name/i);
  user.type(input, "yogie");
  expect(getByText(/hello, my name is yogie/i)).toBeInTheDocument();
});

it("renders props correctly", () => {
  // test jika props benar dirender jika ada perubahan
  const { getByText, rerender } = render(
    <NewComponents data={"a test props"} />
  );
  expect(getByText("a test props")).toBeInTheDocument();
  rerender(<NewComponents data={"a new test props"} />);
  expect(getByText("a new test props")).toBeInTheDocument();
});

it("renders fetched data correctly", async () => {
  // test jika fetching data berhasil
  const { getByText } = render(<NewComponents />);
  expect(getByText(/loading.../i)).toBeInTheDocument();
  await waitForElementToBeRemoved(() => getByText(/loading.../i));
  expect(getByText(/leanne graham/i)).toBeInTheDocument();
  expect(getByText(/Ervin Howell/i)).toBeInTheDocument();
  expect(getByText(/Clementine Bauch/i)).toBeInTheDocument();
  screen.debug();
});
