import styles from "../styles/component.module.scss";
import style from "../styles/home.module.scss";
import { useEffect, useState } from "react";

const NewComponents = ({ ...props }) => {
  const { data } = props;
  const [user, setUser] = useState([]);
  console.log(user);
  const fetchData = (url) => {
    fetch(url)
      .then((res) => res.json())
      .then((data) => setUser(data));
  };
  useEffect(() => {
    fetchData("https://jsonplaceholder.typicode.com/users");
  }, []);
  return (
    <div
      className={`${styles["component-title"]} ${styles["component-padding"]}`}
    >
      <h1 className={style.componentBackground}>This is a new component</h1>
      <h2>{data}</h2>
      <div>
        <h1>Fetched Data</h1>
        {user && user.length
          ? user.map((item, index) => {
              return (
                <div key={index}>
                  <p>{item.name}</p>
                </div>
              );
            })
          : "Loading..."}
      </div>
    </div>
  );
};

export default NewComponents;
